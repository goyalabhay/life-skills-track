# Good Practices for Software Development

## Q1. What is your one major takeaway from each one of the 6 sections. So 6 points in total.

* Gaining a thorough understanding of the customer Requirements are the starting point for software development, thus before continuing, it is crucial to carefully define them and have the client or the assigning authority to certify their accuracy. By doing this, everyone will be in agreement.
* When you and the team are completing the project Communication with the team members at the appropriate time is essential because other team members may be counting on our efforts. Therefore, letting them know about any delays or deadlines we might miss will enable them to modify the schedule.
* When we are stuck, we must ask for help, but the manner in which we do so is equally important. In order to help the person comprehend the issues, it is best to describe the issues using a variety of tools, such as code snippets, screenshots, screen captures, and logs.
* Working as a team and getting to know your coworkers can help you be more productive because there won't be any communication issues that would slow down the flow of information.
* We should exercise caution when conversing with our teammates because excessive talk can make people uncomfortable while little communication can lead to harmful relationships amongst teammates.
* You need to be involved and concentrated in order to be successful at programming. We should make every effort to minimise distractions if we want to maximise our time and get the finest results.

## Q2. Which area do you think you need to improve on? What are your ideas to make progress in that area?

I think I need to work on more efficient methods of validating the project requirements that have been written down. I'll write down the requirements on a piece of paper and confirm them in the actual meeting.
Maintaining a positive working relationship with my teammate may enable me to be more productive by resolving problems more quickly. I can attempt to arrange daily meetings for my staff.
