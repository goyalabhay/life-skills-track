# Prevention of Sexual Harassment

## Q1. What kinds of behaviour cause sexual harassment?

* Unwanted sexual advances
* Sexual comments or jokes
* Sexual intimidation or coercion
* Unwanted physical contact
* Sexual display or exposure
* Sexual slurs or insults
* Invasion of privacy
* Online Harassment

## Q2. What would you do in case you face or witness any incident or repeated incidents of such behaviour?

* Assess your safety
* Document the incidents
* Talk to someone you trust
* Review company policies or guidelines
* Report the incidents
* Seek external support
* Support others

## Q3. Explains different scenarios enacted by actors.

* **Workplace Harassment:** Kavita works in an office where her coworker, rohit, frequently sends explicit texts, makes improper sexual remarks about her attractiveness, and inappropriately touches her. This is regarded as sexual harassment and makes Kavita's workplace uncomfortable.

*  **Street harassment:** Usha is being catcalled by a group of males as she crosses the street, who are making graphic and vulgar remarks about her body. Usha feels uncomfortable and threatened by their unwelcome, disrespectful behaviour. This is an illustration of sexual harassment that takes the form of street harassment.

*  **Cyberbullying:** Emma is a proponent of an online gaming community. She is, however, subjected to ongoing abuse from another gamer who sends her lewd messages, criticises her gender, and threatens her with sexual assault. Online harassment of this nature is regarded as sexual harassment.

*  **Harassment in an Academic Setting:** Maya notes that David, a classmate, frequently enters her personal space, touches her improperly, and makes unwelcome approaches towards her during a lecture. Maya's capacity to concentrate and participate in her studies is impacted by this behaviour in addition to making the learning atmosphere uncomfortable.

*  **Harassment at a Social Event:** Lisa goes to a party with her pals. A man named Mike constantly tries to dance sensually with her during the occasion.

## Q4. How to handle cases of harassment?

* Ensure immediate safety
* Encourage open communication
* Document incidents
* Provide support
* Follow organizational policies
* Report the harassment
* Conduct a thorough investigation
* Take appropriate action
* Provide ongoing support and prevention
* Monitor and evaluate

## Q5. How to behave appropriately?

* Respect boundaries
* Use respectful language
* Listen actively
* Treat others equally
* Avoid making inappropriate jokes or comments
* Seek consent
* Be aware of power dynamics
* Educate yourself
* Address inappropriate behavior
* Reflect and learn