# Energy Management

## Manage Energy not Time
## Q1. What are the activities you do that make you relax - Calm quadrant?

* A little meditation session has helped me unwind and clear my head.
* I take a stroll to warm up and to empty my mind of the many things that race through it every night.
* Good chats with my family and friends help me to reduce my stress and focus on more vital tasks.
* My preferred music plays, which soothes my thoughts.

## Q2. When do you find getting into the Stress quadrant?

* I become worried when I have a lot of things to complete and a short deadline.
* I become nervous when I can't perform a task perfectly.
* When I see someone doing anything improperly, I get upset.

## Q3. How do you understand if you are in the Excitement quadrant?

* When I do my project on time, I am happy.
* I love it when people visit me unexpectedly.
* I enjoy going on vacation with my friends and family.

## Sleep is your superpower
## Q4. Paraphrase the Sleep is your Superpower video in detail.

* Sleep is essential to the learning process both before and after learning anything. Getting enough sleep before learning something new primes our brain to process more information.
* According to the speaker's research, those who get enough sleep each night are 40% more productive when learning.
* A 4 hour sleep limit can cause a 75% reduction in natural killer cells, which in reality aid in our body's ability to fend off disease.
* Creating a consistent sleep schedule might enhance our sleep.
* Rapid sleep can be facilitated by keeping both body and room temperatures low.

## Q5. What are some ideas that you can implement to sleep better?

* Lowering my caffeine consumption.
* Maintaining a regular prayer and meditation routine.
* Limiting the number of meals eaten before bed.
* I can get to bed earlier if I don't use my phone right before night.
* I can get a decent night's rest if I set a regular bedtime.

## Brain Changing Benefits of Exercise
## Q6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

* When the speaker went on a kayaking trip by herself, she discovered that she was the worst member of the group she had travelled with. She then decided she would never behave badly again.
* She started working on her health and started working out frequently. She also tried kickboxing, yoga, Zumba, and any other form of exercise that might help her achieve her health goals.
* Literature claims that exercising enhances our moods, vigour, memory, and attentiveness. She became aware of all these changes.
* Exercise can help our prefrontal cortex and hippocampus grow and strengthen.
* According to the speaker, we should exercise for at least 30 minutes, four times per week at the very least. Furthermore, power walking can be done as a kind of exercise without having to go to the gym.

## Q7. What are some steps you can take to exercise more?

* I will make an effort to be more physically active and finish all of my daily duties by myself, without help.
* Make sure to fit in at least 15 to 20 minutes of activity each day.
* Choose to walk when possible to cover short distances rather than using a vehicle.
* I can stay motivated and enjoy my workouts more if I exercise with a friend.