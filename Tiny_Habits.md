# Tiny Habits

## Tiny Habits - BJ Fogg

## Q1. Your takeaways from the video (Minimum 5 points)

* Any routine that keeps us calm or healthy is beneficial.
* Clarify your aspiration.
* Select a pertinent prompt.
* Complement with specific actions.
* Start small.



## Tiny Habits by BJ Fogg - Core Message

## Q2. Your takeaways from the video in as much detail as possible

* Behaviour depends on three factors: motivation, aptitude, and prompt.
* For simple activities, less motivation is required, and vice versa.
* A habit can be easily formed, but it can be difficult to keep up over time.
* The solution is to make the habit as small as possible. Because of this, adopting and establishing it permanently is straightforward.
* The action prompt is the best way to create new habits. This question harnesses our existing enthusiasm to launch a brand-new, small project.
* Learning to celebrate after a task is finished is the most crucial part of creating a habit. It keeps the want to repeat the practise alive.

## Q3. How can you use B = MAP to make making new habits easier?

* As soon as I get out of bed, I'll make my bed.
* When I'm done brushing my teeth, I'll perform five pushups.
* I'll finish my food and go for a little stroll.
* Before turning in for the night, I'll think about one good thing that happened today.
* I'll acquire information and tools to support my new habit.


## Q4. Why it is important to "Shine" or Celebrate after each successful completion of habit?

After every habit is successfully completed, it is crucial to "shine" or celebrate since this reinforces the behaviour and establishes a constructive feedback loop in the brain. Celebrating the accomplishment of a behaviour increases the likelihood that you will carry out that habit in the future because the brain prefers to repeat activities that are connected to satisfying feelings and benefits. Celebrating also fosters a sense of self-worth and self-efficacy, which makes it simpler to take on bigger challenges and objectives. In addition, celebrating can make the habit more joyful, which makes it simpler to keep over time.

## 1% Better Every Day Video

## Q5. Your takeaways from the video (Minimum 5 points)

* The impacts of habits compound; at first, we don't notice much of a difference, but over time, we notice a big change in ourselves.
* If you want better results, focus on the stages involved in achieving your objectives rather than just setting them.
* The best way to change your behaviour is to concentrate on how to achieve your goals rather than what you want to do.
* If you want to create good habits, make them obvious, appealing, easy, and satisfying.
* Regular habits are what lead to success, not sudden changes in behaviour. A decision made on a particularly motivational day won't have any effect the next day since you might forget about it.

## Book Summary of Atomic Habits

## Q6. Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

To form habits, we take a different route. We try to create a habit, and then we pray that after doing this for a while enough, it will stick. We'll incorporate it into our identity.
The identity should take precedence in our thoughts instead. For example, if I want to lose weight and get healthy, I should think of myself as fit and eat properly, refrain from regularly consuming junk food, and exercise frequently.

## Q7. Write about the book's perspective on how to make a good habit easier?

* To make good habits easier, we must remove as many obstacles as possible from our way.
* Make habits appealing.
* Making things simple lowers resistance and prepares our environment for the behaviours we wish to develop. When the friction is decreased, the task is much more likely to be completed.
* We should try to correlate some sort of instant gratification with our habits in order to develop them into ones that are immediately satisfying.


## Q8. Write about the book's perspective on making a bad habit more difficult?

* Make it invisible.
* Make it appear poor.
* Make it harder.
* Make it inadequate.

## Reflection

## Q9. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

* I'm going to start solely consuming wholesome foods going forward.
* I'll forge an identity for myself as a healthy person. I won't attempt to eat junk food every day. Possibly a couple of times a month.

## Q10. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?


* Just before going to bed, I'll put my phone away.
* I'll tuck the phone into my bag before turning in for the evening. I'd try to read a couple of Kindle pages.
* Reading some articles regarding the negative effects of using a phone while lying in bed would help me persuade myself.