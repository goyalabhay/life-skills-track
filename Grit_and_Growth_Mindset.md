# Grit and Growth Mindset

## Grit

## Q1. Paraphrase (summarize) the video in a few lines. Use your own words.

The video is about how IQ alone does not determine a student's performance. Instead passion and perseverance for long-term goals, which is defined as grit, plays a crucial role. Considering factors like family income and test scores, students with more grit are more likely to graduate and excel. Research is needed to test and measure the effectiveness of different approaches in cultivating grit in children.


## Q2. What are your key takeaways from the video to take action on?

My main conclusions after seeing the video are as follows:

* To grow passion and determination for long-term goals by learning from mistakes and being persistent in pursuing them.
* Education requires a motivational and psychological viewpoint.
* Learning skills can be developed with work and are not fixed.

## Introduction to Growth Mindset

## Q3. Paraphrase (summarize) the video in a few lines in your own words.

* **Working-place harassment:** Kavita works in an office where one of her coworkers, rohit, routinely sends graphic SMS, makes inappropriate sexual comments about her appearance, and touches her in an uncomfortable way. This is considered sexual harassment and unsettling for Kavita's workplace.


* **Harassment in public places:** As Usha crosses the street, a bunch of boys start catcalling her and making crude and obscene comments about her body. Usha is threatened and uneasy by their disrespectful, uninvited actions. This is an example of street harassment that includes sexual overtones.


* **Internet bullying:** Emma supports a community for internet gamers. However, a different gamer consistently harasses her, sending her filthy messages, criticising her gender, and threatening to sexually assault her. Such online harassment is considered to be sexual harassment.

* **Harassment in an Academic Environment:** Maya reports that during class, David, a classmate, constantly invades her personal space, touches her inappropriately, and makes unwanted advances. This activity not only disrupts the learning environment but also affects Maya's ability to focus and participate in her studies.


* **Inappropriate Behaviour at a Social Event:** Lisa and her friends attend a party. Throughout the event, a man named Mike keeps attempting to dance sensually with her.

## Q4. What are your key takeaways from the video to take action on?

Important ideas to remember from the videos are:

* Adopt a growth mentality
* Characteristics of mindsets
* Importance of a growth mindset 
* Four essential ingredients for growth
* Building a culture of learning

## Understanding Internal Locus of Control

## Q5. What is the Internal Locus of Control? What is the key point in the video?

The idea that people have control over their lives and are accountable for the results they encounter is known as the internal locus of control. It places a strong emphasis on individual effort and deeds as the key determinants of success or failure.

The video's focal points are:

* To stay motivated, one must have an internal locus of control.
* According to the study described in the movie, people who believed that their hard work and effort had contributed to their success showed higher levels of drive and tenacity.
* In contrast, those who thought their success came from outside sources, such as being born intelligent or gifted, showed reduced motivation and a lack of interest in undertaking difficult activities.
* The video makes the case that assuming accountability for one's actions and accomplishments can enhance motivation and a person's sense of control over their life.

## How to build a Growth Mindset

## Q6. Paraphrase (summarize) the video in a few lines in your own words.

A growth mentality starts with the guiding principle of life, which is having faith in our capacity for problem-solving. We can pursue lifetime growth and improvement if we have faith that we can solve problems and get better.
To foster a stronger growth attitude, we should also challenge our presumptions. Don't let your existing knowledge, skills, and talents restrict your future goals because what you can achieve today doesn't necessarily reflect what you can do in the future.

## Q7. What are your key takeaways from the video to take action on?

The main points are:

* Have faith in your capability to solve problems
* Question your assumptions
* Design your own learning path
* Embrace the struggle
* Build resilience.
* Seek feedback and take advice from criticism

## Mindset - A MountBlue Warrior Reference Manual

## Q8. What are one or more points that you want to take action on from the manual?

I wish to act on the following points from the manual:

* My learning is entirely my responsibility.
* Until I can perform tasks while dozing off, I will be mastery-focused. If I'm awakened at three in the morning, I'll calmly address the issue like James Bond before going back to sleep.
* My enthusiasm never wanes. When I meet new people or take on new challenges, I will be smiling.


