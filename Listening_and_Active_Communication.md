# Listening and Active Communication

## Q1. What are the steps/strategies to do Active Listening?

* Give your full attention
* Be present and non-judgmental
* Practice empathy
* Provide verbal and non-verbal cues
* Ask clarifying questions
* Summarize and reflect

## Q2. According to Fisher's model, what are the key points of Reflective Listening?

Reflective listening, according to Fisher's model, involves actively listening to the speaker and then reflecting their thoughts and feelings back to them.
* Pay attention
* Listen without judgment
* Reflect the content
* Reflect the feelings
* Use non-verbal cues
* Clarify and seek confirmation
* Avoid giving advice or personal opinions

## Q3. What are the obstacles in your listening process?

* Avoid getting distracted by your own thoughts
* Focus on  the topic or the speaker instead
* Try not to interrupt the other person
* Let them finish and then respond

## Q4. What can you do to improve your listening?

* Be present and focused
* Avoid interrupting
* Practice active listening
* Control internal distractions
* Seek clarification
* Paraphrase and Summarize
* Show empathy and understanding
* Practice patience
* Reflect on your listening experiences
* Seek feedback

## Q5. When do you switch to Passive communication style in your day to day life?

* Maintaining harmony in relationships
* Respecting other's preferences
* Avoiding confrontation or aggression
* Cultural or social norms

## Q6. When do you switch into Aggressive communication styles in your day to day life?

* High stress situations
* Personal conflicts and disagreements
* Lack of effective communication skills
* Cultural or social conditioning

## Q7. When do you switch into Passive Aggressive communication styles in your day to day life?

* Fear of direct confrontation
* Power dynamics or perceived lack of control
* Assertiveness challenges
* Emotional avoidance


## Q8. How can you make your communication assertive? 

* Use "I" statements
* Be clear and direct
* Practice active listening
* Maintain confident body language
* Set and respect boundaries
* Manage emotions effectively
* Practice assertive responses
* Seek win-win solutions
* Reflect and learn from experiences


